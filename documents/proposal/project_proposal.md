# Biological Data Mining 18/19: Project
### Germana, Giuseppe, Lorenzo, Stefano

---

Gene expression data is not always available for organisms other than the model one. Thus, being able to transfer an expanded LGN from the model organism to the one it is being studied could be of great help.

A first proposal reads as follows. On a very high level, the aim of the first part of the project is to collect gene expression data for Arabidopsis *thaliana* possibly using the ArrayExpress and GEO resources. The objective is to collect as much data as possible, meaning that this will require cleaning and integrating different datasets, possibly from the same sequencing platform to avoid normalization issues and to accelerate this first part of the project. While the details are still not clear, we will try to collaborate with the other group in order to either have a different type of data (Expression profiling by array *vs* RNA seq.) or to create a common dataset to be further used.

In parallel, the group will also compile a list of possible LGNs to be then expanded using NESSRA in the second part of the project. Having a ranking will allow us to focus on more interesting hormonal pathways and/or to test the method in more then one case. Indeed, a possible way to validate our idea(s) is to use the entire dataset, but to split the LGNs into *training* and validation.

Once the results from NESSRA have been collected and analyzed, the next steps are still to be defined. The aim is to compare the expanded GLNs in Arabidopsis *thaliana* and grapevine. We expect them to be *different*, although we cannot properly quantify it at the moment. Nonetheless, a few ideas came to mind. The first, most simple thing to perform would be to compare the sequences of the genes in the pairs of networks: this could be easily done with an appropriate version of BLAST. In case this will not allow us to properly *go* from one network to the other, the idea is to perform a screening of the available literature on the topic. Moreover, it would be interesting trying to integrate information other than the gene sequence, for instance using protein networks. A possible alternative or complementary path would be to compare gene expression networks in a multi-dimensional space to extract similarities and differences between organisms: this would involve data visualization, dimensionality reduction and clustering.

Regarding NESSRA and its methodology, the group was also wondering whether the way conditional independence is computed could influence drastically the resulting list of genes. Thus, other methods that avoid the use of significance levels could be searched.
